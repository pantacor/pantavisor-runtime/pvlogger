/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sys/socket.h>
#include <sys/un.h>

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include <unistd.h>

#define PV_CTRL_LOG_PATH "/pantavisor/pv-ctrl-log"
#define PV_LOG_VERSION 0

typedef enum {
	FATAL,
	ERROR,
	WARN,
	INFO,
	DEBUG,
	ALL
} log_level_t;

struct pv_logger_msg {
	int version;
	int len;
	char buffer[0];
};

inline static void pv_log(const char *platform, const char *file, log_level_t level, const char *msg, ...)
{
	va_list args;
	int fd, len;
	char *fmtd = NULL, *buf = NULL;
	struct sockaddr_un addr;
	struct pv_logger_msg pv_logger_msg;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("socket error");
		goto out;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, PV_CTRL_LOG_PATH, sizeof(PV_CTRL_LOG_PATH));

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("connect error");
		goto out;
	}

	// prepare formatted log message
	va_start(args, msg);
	len = snprintf(NULL, 0, msg, args);
	fmtd = calloc(1, len);
	vsnprintf(fmtd, len, msg, args);
	va_end(args);

	// prepare pv-ctrl-log friendly message
	len += strlen(platform) + strlen(file) + 5;
	pv_logger_msg.version = PV_LOG_VERSION;
	pv_logger_msg.len = len;
	buf = calloc(1, len + sizeof(pv_logger_msg));
	memcpy(buf, &pv_logger_msg, sizeof(pv_logger_msg));
	snprintf(buf + sizeof(pv_logger_msg), len, "%d%c%s%c%s%c%s%c", level, '\0', platform, '\0', file, '\0', fmtd, '\0');

	if (write(fd, buf, pv_logger_msg.len + sizeof(pv_logger_msg)) < 0) {
		perror("write error");
		goto out;
	}

out:
	close(fd);

	if (fmtd)
		free(fmtd);
	if (buf)
		free(buf);
}
