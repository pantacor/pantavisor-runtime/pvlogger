# pvlogger

Pantavisor library and command to help you sending logs from the container side.

## How to start using the pvlogger library

Just include the pvlogger.h in your project. Then, the API only has an entry point:

```
#include "pvlogger.h"

...
pv_log("platform-name", "file-name.log", DEBUG, "test %s", 123);
```

## How to start using the pvlogger command

To use the pvlogger command, add the following snippet to your dockerfile:

**X86_64 (static binary)**
```
FROM registry.gitlab.com/pantavor/pantavisor-runtime/pvlogger:X86_64-master as pvlogger

COPY --chown=0:0 --from pvlogger /usr/local/bin/pvlogger /usr/local/bin/pvlogger
```

**ARM 32-bit (static binary)**
```
FROM registry.gitlab.com/pantavor/pantavisor-runtime/pvlogger:ARM32V6-master as pvlogger

COPY --chown=0:0 --from pvlogger /usr/local/bin/pvlogger /usr/local/bin/pvlogger
```
